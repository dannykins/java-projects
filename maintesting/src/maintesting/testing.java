package maintesting;

public class testing {

	public static void main(String[] args) {
		//here's the main where the actual things will happen
		String sayHi = "hi, my homies.";
		int min = 0;
		
		sayHi(sayHi);
		
		System.out.println("What would you like the program to say?");
		
		numbersTime(min);
		
		System.out.println(min);
		
		italy(sayHi, min);
		
		sidething.main(args);
	}
	
	static void sayHi(String sayHi) {
		System.out.println(sayHi);
	}

	
	static void italy(String sayHi, int min) {
		sayHi = sayHi.replace("hi", "yo");
		System.out.println(sayHi+min);
		
	}
	
	//outside methods can't change the variables kept in the mainmethod, they can only change and use the new variables within the specific outside method.
	static int numbersTime(int min) {
		min = 1+3;
		
		return min;
	}
}
