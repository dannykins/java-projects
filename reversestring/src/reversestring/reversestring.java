package reversestring;

public class reversestring {

	public static void main(String[] args) {
		
		String word = "daniel";
		
		String r1 = order(word);
		System.out.println("When put in correct order, the word or phrase is: "+r1);
		String r2 = reverse(word);
		System.out.println("When reversed, the word or phrase is: "+r2);
	}

	//lists out the letters in order
	public static String order(String o) {
		char [] letters = new char[o.length()];
		
		System.out.println("The letters is correct order are:");
		
		for (int i=0; i<o.length(); i++) {
			System.out.println(o.charAt(i));
		}
		return o;
	}
	
	//reverse the letters
	public static String reverse(String s) {
		
		//set up the length of the given word(s)
		char [] letters = new char[s.length()];
		
		System.out.println("The letters is reverse order are:");
		
		int letterIndex = 0;
		//performs the reversal
		for (int i= s.length()-1; i>=0; i--) {
			letters[letterIndex] = s.charAt(i);
			letterIndex++;
			System.out.println(s.charAt(i));
		}
		
		//combine the reversed letters into a single string
		String reverse = "";
		for (int i = 0; i < s.length(); i++) {
			reverse = reverse + letters[i];
		}
		
		//returned "reverse" in the form of "String" to the top.
		return reverse;
	}
}
