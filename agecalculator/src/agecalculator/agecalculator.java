package agecalculator;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

public class agecalculator {

	public static void main(String[] args) {
		
		//what allows user input
		//"System.in" is input via the console
		Scanner scan = new Scanner(System.in);
		
		System.out.println("What is your birth year? (e.g. 1988)");
		//set up this specific input.
		int a = scan.nextInt();
		
		System.out.println("What is your birth month? (e.g. 7 for July)");
		//set up this specific input.
		int b = scan.nextInt();
		
		System.out.println("What is your birth day? (e.g. 19)");
		//set up this specific input.
		int c = scan.nextInt();
		
		LocalDate today = LocalDate.now();
		LocalDate birthday = LocalDate.of(a, b, c);
		int years = Period.between(birthday, today).getYears();
		int months = Period.between(birthday, today).getMonths();
		int days = Period.between(birthday, today).getDays();
		
		System.out.println(today);
		System.out.println(birthday);
		System.out.println("You are " + years + " years, " + months + " months, and " + days + " days old.");

	}

}
