package adding;

import java.util.Scanner;

public class Adding {

	public static void main(String[] args) {
		
		//what allows user input
		//"System.in" is input via the console
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Type a number and press enter.");
		//set up this specific input.
		int a = scan.nextInt();
		//print out to confirm
		System.out.println("You typed in the number " + a);
		
		System.out.println("Type a number and press enter.");
		//set up this specific input.
		int b = scan.nextInt();
		//print out to confirm
		System.out.println("You typed in the number " + b);
		System.out.println("The answer to " + a + " + " + b + " is...");
		System.out.println(a+b);

	}

}
