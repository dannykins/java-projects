package superman;

class vehicle {
	
	int maxspeed = 120;
	
	public void vroom() {
		System.out.println("vroom vroom");
	}
	
	//constructor
	vehicle() {
		System.out.println("We are in the Vehicle constructor. I am the superconstructor.");
	}
	
	//another constructor
	vehicle(int maxspeed) {
		System.out.println("We are in the Vehicle constructor, but this time with speed.");
		//this keyword applies to the
		this.maxspeed = maxspeed;
	}
}

//because this "extends" from the vehicle class, this is a subclass. vehicle is the super class. so "super keyword" is used to 
//call the "super" class while in a subclass, especially when a variable has two values (e.g. vehicle maxspeed is 120, car is 100)
class car extends vehicle {
	
	int maxspeed = 100;
	
	public void vroom() {
		//prints out "120" over "100"
		System.out.println(super.maxspeed);
		System.out.println("skkkkkkkk!!");
		//prints out "vroom vroom" over "skkkkk". In an ideal code, this one line would be alone, but we want other examples lol
		super.vroom();
	}
	
	//this is a subconstructor
	car() {
		//this will run the vehicle constructor print. this always needs to be first, otherwise things will break
		super();
		
		System.out.println("--------------");
		//this calls both, since car extends vehicle
		System.out.println("We are in the Car constructor. Because I extend vehicle, I am a subconstructor.");
	}
}
