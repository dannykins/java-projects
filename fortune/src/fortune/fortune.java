package fortune;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class fortune {
	
	static String[] cue = {"Red Square",
			"Blue Square",
			"Yellow Square",
			"Blue Triangle",
			"Red Triangle",
			"Yellow Triangle",
			"Red Circle",
			"Blue Circle",
			"Yellow Circle",
			"Red Waves",
			"Blue Waves",
			"Yellow Waves",
			"Red Flower",
			"Blue Flower",
			"Yellow Flower"};
	
	public static void main(String[] args) throws InterruptedException {
		
		System.out.println("ADVISOR INSTRUCTIONS:\n" + "Enter number of rounds with Patient:");
		
		Scanner s = new Scanner(System.in);
		
		int i = s.nextInt();
		
		int correct = 0;
		
		String[] answers = new String[i];
		
		System.out.println("A colored object will be selected by the machine. Take a few seconds to focus on the object and its color in your mind.");
		
		Thread.sleep(1000);
		
		System.out.println("Then have the Patient look into your eyes and guess the object and its color.");
		
		Thread.sleep(1000);
		
		System.out.println("Notate whether the Patient answers correctly or not, and continue until the experiment is completed.");
		
		Thread.sleep(1000);
		
		System.out.println("Submit results to your Superior(s).");
		
		for(int l = 0; l < i; l++) {
			
			System.out.println("Round "+(l+1)+"\n"+"\n"+"The object is: ");
		
			Random rand = new Random();
			int r = rand.nextInt(cue.length);
			System.out.println(cue[r]);
			
			//Thread.sleep(5000);
			
			System.out.println("Did the Patient answer correctly? (Y/N)");
			
			String a = s.next();
			
			String b = a.toLowerCase();
			
			answers[l] = b;
			
			if (b.contains("y")) {
				correct += 1;
			}
			
			Thread.sleep(2000);
			
			
		}
		
		System.out.println("The Experiment has concluded. The answers are listed as follows:");	
		
		System.out.println(Arrays.toString(answers));
		
		int ratio = (100*correct)/i; 
		
		System.out.println("Patient was correct "+correct+" times out of "+i+" times ("+ratio+"%).");
		
		System.out.println("Please present the results to your Superior(s)");

	}

}
