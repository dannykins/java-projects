package findduplicates;

public class findduplicates {

	public static void main(String[] args) {

		String sentence0 = "What letters are duplicated in this sentence?";
		System.out.println(sentence0);
		
		//this is to hold the individual characters
		String characters = "";
		
		//this is to hold any found duplicates
		String duplicates = "";
		
		//convert sentence to have no spaces (since that would return as a duplicate!)
		String sentence = sentence0.replace(" ", "");
		
		//0 will return "H", 16 would return "t"
		//sentence.charAt(0);
		//System.out.println(sentence.charAt(0));
		
		//to find the duplicates, separate the
		for (int i = 0; i < sentence.length(); i++) {
			
			//this would print out each character on its own line
			//System.out.println(sentence.charAt(i));
			
			//this will add each new character into the string
			//characters += sentence.charAt(i);
			//System.out.println(characters);
			
			//separate the current character into another variable to compare it with
			String current = Character.toString(sentence.charAt(i));
			
			//if statement to run through the current character versus the already-run characters
			if (characters.contains(current)) {
				//if statement to cut out duplicates that have already been found!
				if (!duplicates.contains(current)) {
				
					duplicates += current + ",";
				}
			}
			//add in current character to string to compare following characters!
			characters += current;	
			
		}
		
		System.out.println(duplicates);
	}

}
