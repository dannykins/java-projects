package simpletest;

import java.util.Random;

public class rollingdice {

	public static void main(String[] args) {
		
		Random rand = new Random();
		
		//roll a random number within the given number (6).
		//need the +1, cause just 6 means 0-5. So add one to remove the 0, add the 6!
		int x = rand.nextInt(6) + 1;
		
		//show the results of the roll!
		System.out.println("You rolled a: " + x);

	}

}
