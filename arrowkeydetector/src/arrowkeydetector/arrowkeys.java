package arrowkeydetector;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

//import javax.swing.JFrame;
//import javax.swing.JPanel;
//if you wanted to import everything from swing instead of individual imports:
import javax.swing.*;

public class arrowkeys {

	public arrowkeys() {
	
		//bring up a new window
		JFrame frame = new JFrame();
		//sets it to be visible
		frame.setVisible(true);
		//sets it to close when you hit close 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//set size of the window in pixels
		frame.setSize(400, 400);
		//lets you click on it
		frame.setFocusable(true);
		
		//create a panel
		JPanel panel = new JPanel();
		//create labels
		JLabel up = new JLabel();
		JLabel down = new JLabel();
		JLabel left = new JLabel();
		JLabel right = new JLabel();
		//give visible text to each label
		up.setText("Up: 0");
		down.setText("Down: 0");
		left.setText("Left: 0");
		right.setText("Right: 0");
		
		//now add the labels to the panel
		panel.add(up);
		panel.add(down);
		panel.add(right);
		panel.add(left);
		
		//now add in the actual detection for the keys within the frame
		frame.addKeyListener(new KeyListener() {
			
			//set up int variables to collect the number of presses.
			//gotta start at 1, otherwise it won't register the first key press.
			int upCount = 1;
			int downCount = 1;
			int leftCount = 1;
			int rightCount = 1;

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyPressed(KeyEvent e) {
				//log the key pressed (each key has a specific code)
				int keyCode = e.getKeyCode();
				//statement to determine the code to the key
				switch(keyCode) {
					case KeyEvent.VK_UP:
						//gotta remember to convert the upcount++ int to a string so it can print
						up.setText("Up: " + Integer.toString(upCount++));
						break;
					case KeyEvent.VK_DOWN:
						//gotta remember to convert the upcount++ int to a string so it can print
						down.setText("Down: " + Integer.toString(downCount++));
						break;
					case KeyEvent.VK_LEFT:
						//gotta remember to convert the upcount++ int to a string so it can print
						left.setText("Left: " + Integer.toString(leftCount++));
						break;
					case KeyEvent.VK_RIGHT:
						//gotta remember to convert the upcount++ int to a string so it can print
						right.setText("Right: " + Integer.toString(rightCount++));
						break;
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//now add the panel to the window
		frame.add(panel);
		
		
	}
	public static void main(String[] args) {
		new arrowkeys();

	}

}
