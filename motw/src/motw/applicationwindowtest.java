package motw;

import java.awt.EventQueue;

import javax.swing.*;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;

public class applicationwindowtest {

	private JFrame frame;
	private JButton exp1butt;
	
	//this is the stuff for experiment 1. It's outside of all the other things, so should be usable by all and any constructor/etc
	String[] exp1img0 = {"fbi logosmall.png", "gov logosmall.png", "gov logosmall2.png", "gov logosmall3.png"};
	Random rand = new Random();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					applicationwindowtest window = new applicationwindowtest();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 */
	public applicationwindowtest() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		JTextArea ta = new JTextArea();
		ta.setRows(5);
		ta.setEditable(false);
		ta.setFont(new Font("Monospaced", Font.BOLD, 34));
		ta.setText("DARPA TOOL KIT");
		JPanel p1 = new JPanel();
		p1.setLayout(new BoxLayout(p1, BoxLayout.X_AXIS));
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon("F:\\MotW\\Unfound Stuff\\fbi logosmall.png"));
		p1.add(lblNewLabel_2);
		p1.add(ta);
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		
		
		
		JTabbedPane tp = new JTabbedPane(JTabbedPane.TOP);
		tp.setBounds(0, 0, 700, 400);
		
		tp.add("Main",p1);
		
		tp.add("Tab 2",p2);
		String imagefull = "F:\\\\MotW\\\\Unfound Stuff\\\\"+exp1img0[1];
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(imagefull));
		p2.add(lblNewLabel_1);
		JButton exp1butt = new JButton("New");
		exp1butt.setActionCommand("button_click");
		exp1butt.addActionListener((ActionListener) this);
		p2.add(exp1butt);
		
		tp.add("Tab 3",p3);
		
		frame.getContentPane().add(tp);
		
		
		

	}
	
	//the action for experiment 1 button
	public void actionPerformed(ActionEvent e1) {
		
		switch(e1.getActionCommand()) {
		case("button_click"):
			JOptionPane.showMessageDialog(null, "The button clicking worked!");
		break;
		}
		//if (Action.contentEquals("new"));
		//int r = rand.nextInt(exp1img0.length);
		//String imagefull = "F:\\\\MotW\\\\Unfound Stuff\\\\"+exp1img0[r];
		//System.out.println(r);
		//System.out.println(imagefull);
		
		
	}
}
