package motw;


import java.util.Random;
import javax.swing.*;
import java.awt.*;


public class gui {
	
	//window info in it's own thing so it'll hopefully be usable outside of the main
	

		
		//actual window
		static JFrame window = new JFrame();
		
		
		//add in text area for text display
		static String text = " ";
		static JTextArea textArea2 = new JTextArea(text, 25, 50);

	
	//provides an optimizable time delay
	public static void d(int delaytime) {
		try {Thread.sleep(delaytime);}
		catch(InterruptedException ex) {Thread.currentThread().interrupt();}
		
	}
	
	//print with two enters
	public static void s2(String phrase) {
		textArea2.append("\n"+"\n"+phrase);
		textArea2.setCaretPosition(textArea2.getDocument().getLength() - 1);
		
	}
		
	//print with one enter
	public static void s1(String phrase) {
		textArea2.append("\n"+phrase);
		textArea2.setCaretPosition(textArea2.getDocument().getLength() - 1);
		
	}	
	
	
	//this is a fake loading percentage
	public static void lt(String load) {
		for (int i = 0; i < 101 ; i++) {
			
			textArea2.setText(load + i + "%");
			textArea2.setCaretPosition(textArea2.getDocument().getLength() - 1);
			
			int r = new Random().nextInt(2);
			int r2 = new Random().nextInt(1000);
			
			System.out.println(r*r2);
			
			d(r*r2);
		}
		
	}
	
	//username input
	public static void user() {
		
		//version 1 - only works within console, kept for posterity
		//Scanner scan1 = new Scanner(System.in);
		//s("Username: ");
		//String username = scan1.nextLine();
		
		//version 2 - dialog box
		JOptionPane userpane = new JOptionPane();
		String username = JOptionPane.showInputDialog(userpane, "Due to classified information therein,\n" + "incorrect input may result in an error\n"+ "report and in immediate termination\n" + "of the program.\n\n"+"USERNAME: ");
		
		//System.out.println(username);
		

		if (username.contentEquals("test")) {
			s1(".");
			d(1000);
			s1(".");
			d(1000);
			s1(".");
			d(1000);
			s1(".");
			d(1000);
			s1(".");
			d(1000);
			s1("Username has been accepted.");
			d(2000);
		}
		else {
			close();
		}
		//return username;
	}
	
	
	//password input
	public static void pass() {
		
		//version 2 - dialog box
		JOptionPane passpane = new JOptionPane();
		String password = JOptionPane.showInputDialog(passpane, "Due to classified information therein,\n" + "incorrect input may result in an error\n"+ "report and in immediate termination\n" + "of the program.\n\n"+"PASSWORD: ");
	
				
		if (password.contentEquals("test")) {
			d(1000);
			s1(".");
			d(1000);
			s1(".");
			d(1000);
			s1(".");
			d(1000);
			s1(".");
			d(1000);
			s1(".");
			d(1000);
			s1("Password has been accepted.");
			d(2000);
		}
		else {
			close();
		}
		
	}
	
	
	
	//if something goes wrong, this closes the program
	public static void close() {
		d(10000);
		textArea2.setText("ERROR AND/OR UNAUTHORIZED ACCESS DETECTED.");
		textArea2.setCaretPosition(textArea2.getDocument().getLength() - 1);
		d(1000);
		s2("ERROR REPORT INITIATED.");
		d(3000);
		s2(".");
		d(1000);
		s2(".");
		d(1000);
		s2(".");
		d(1000);
		s2(".");
		d(1000);
		lt("ERROR REPORT SENDING.");
		d(3000);
		s2("ERROR REPORT RECEIVED.");
		d(3000);
		s2("ERROR REPORTING COMPLETED.");
		d(3000);
		s2("INITIATING EXIT SEQUENCE IN:");
		d(1000);
		s2("5");
		d(1000);
		s2("4");
		d(1000);
		s2("3");
		d(1000);
		s2("2");
		d(1000);
		s2("1");
		d(1000);
		System.exit(0);		
	}
	
	
	// this is the main file!!!!
	public static void main(String[] args) {

		//opening dialog box
		JOptionPane option = new JOptionPane();
		JOptionPane.showMessageDialog(option,"CLASSIFIED//FOR OFFICIAL USE ONLY UNDER EXECUTIVE ORDER 12958\n\n"+"Unauthorized Usage and Access will be Detected and Prosecuted with up to 10 years in Federal Prison.","W A R N I N G",JOptionPane.WARNING_MESSAGE);
		
		//window details
		window.setVisible(true);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setSize(800, 600);
		window.setLocationRelativeTo(null);
		window.setTitle("CLASSIFIED//FOR OFFICIAL USE ONLY");
		window.setResizable(false);
		window.setLayout(new FlowLayout());
		
		//text box details
		JScrollPane scrollPane = new JScrollPane(textArea2, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		textArea2.setLineWrap(true);
		window.add(scrollPane);
		window.pack();
		
		d(3000);
		
		String[] windowtext = {" ","L", " O", " A", " D"," I"," N"," G"," .", " .", " .", " .", " ."};
		
		String text = windowtext[0];
		textArea2.setText(text);
		
		int size = windowtext.length;
		
		for (int i = 1; i < size ; i++) {
			text = windowtext[i];
			textArea2.append(text);
			
			d(1000);
		}
		
	
		lt("L O A D I N G . . . . . ");
		
		d(3000);
		
		s2("//USA SECRET");
				
		d(1000);
		
		s2("SECRET//SI/TK//NOFORN//X1");
		
		d(1000);
		
		s2("SECRET//ORCON,PROPIN//1231");
		
		d(1000);
		
		s1("MMMMMMMMMMMMMMMMMMMMMM");
		
		s1("MWX000000000000000000NM");

		s1("K;                  cN");
		
		s1("Xc;dddo coxxxd;. ,l,oN");
		
		s1("K:lKxc: l0kllOO, :O:lN");
		
		s1("K:lKxc: l0kllOO, :O:lN");
		
		s1("K;lO,   ;O:  c0; cO:cN");
		
		s1("K;lO,   ;O:  c0; cO:cN");
		
		s1("K;lKd:  :0kclkx. :O:lN");
		
		s1("K:lKko; :0OodOd. :O:lN");

		s1("K;lO,   ;O:  cO; cO:lN");
		
		s1("K;lO,   ;O:  cO; cO:lN");
		
		s1("K;lO,   ;O:..cO; cO:lN");
		
		s1("K;:x'   'kOxkOo. ;x;cN");

		s1("K;                  cN");
		
		s1("NkllllllllllllllllloOW");
		
		s1("MMMMMMMMMMmMMMMMMMMMMM");
		
		d(3000);
		
		s2("This is a secured and monitored Federal Government system.");
		
		d(1000);
		
		s2("Unauthorized access is strictly prohibited.");
		
		d(1000);
		
		s1("All activity is fully monitored.");
		
		d(1000);
		
		s1("Individuals who attempt to gain unauthorized access or attempt any modification of information on this system is subject to criminal prosecution.");
		
		d(1000);
		
		s1("All persons who are hereby notified that use of this system constitutes as consent to monitoring and auditing.");
		
		d(6000);
		
		s2("INITIATING LOGIN SEQUENCE");
		
		d(3000);
		
		s1(".");
		
		d(1000);

		s1(".");
		
		d(1000);

		s1(".");

		d(1000);
		
		user();
		
		pass();
		
		d(1000);
		
		s2("P R O C E S S I N G");
		
		d(5000);
		
		s2("Welcome, ADMIN-JOHN-WHITE.");
		
		d(1000);
		
		s2("INITIATING MAIN WINDOW.");
		
		d(3000);
		
//		window.setVisible(false);
		
//		motw.mainframe.main(args);
		
		//meat of the application.
		textArea2.setText("DARPA TESTKIT");
		textArea2.setCaretPosition(textArea2.getDocument().getLength() - 1);
		d(1000);
		s2("Remote-Viewing and Parapsychology Analysis Experimental Testkit");
		d(1000);
		s2("");
		s2("Available Options:");
		s2("1: Input New Patient");
		s2("2: Experiment A: ");
		s2("");
		
		
		//experiment A
		textArea2.setText("Experiment A");
		textArea2.setCaretPosition(textArea2.getDocument().getLength() - 1);
		d(1000);
		s2("Experiment A");

	}

}
